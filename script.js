class Employee {
    constructor(name, age, salary) {
        this._name = name;
        this._age = age;
        this._salary = salary;
    }

    get name() {
        return this._name;
    }

    set name(name) {
        this._name = name;
    }
    
    get age() {
        return this._age;
    }

    set age(age) {
        this._age = age;
    }

    get salary() {
        return this._salary;
    }

    set salary(salary) {
        this._salary = salary;
    }
}

class Programmer extends Employee {
    constructor(name, age, salary, lang) {
        super(name, age, salary);
        this._lang = lang;
    }

    get salary() {
        return this._salary * 3;
    }

    get lang() {
        return this._lang
    }

    set lang(lang) {
        this._lang = lang;
    }   
}

let user = new Employee ('Dick', 14, 500);
console.log(user);

let firstProg = new Programmer('Bob', 43, 1000, ["PHP", "JS"]);
let secondProg = new Programmer('Bill', 28, 2000, ["JS", "Python", "Java"]);
let thirdProg = new Programmer('Jack', 14, 1500, ["JS", "Python", "Java"]);
let fourthProg = new Programmer('Michael', 22, 500, ["Python"]);

console.log (firstProg, secondProg, thirdProg, fourthProg);

console.log(`${firstProg.name}'s new salary (firstProg): ${firstProg.salary}`);
console.log(`${secondProg.name}'s new salary (secondProg): ${secondProg.salary}`);
console.log(`${thirdProg.name}'s new salary (thirdProg): ${thirdProg.salary}`);
console.log(`${fourthProg.name}'s new salary (fourthProg): ${fourthProg.salary}`);

let fifthProg = new Programmer('Michael', 22, 1000, ["JS", "Python"]);

console.log (fifthProg);
console.log(`${fifthProg.name}'s new salary (fifthProg): ${fifthProg.salary}`);